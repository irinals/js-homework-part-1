// Рендерим чат
renderChat();
function renderChat() {
  axios.get('/api/chat/message')
	.then(function (response) {
    let dataReverse = JSON.parse(response.request.response).reverse();
    if (!dataReverse.length) return;
    for (let i = 0; i < dataReverse.length; i++) {
        let newMessage = dataReverse[i];
        addItemToDOM(newMessage);
      }
	})
	.catch(function (error) {
    console.log(error);
	});
}

function addItemToDOM(newMessage) {
  let chat = document.getElementById('chat');
  let item = document.createElement('li'); 
  let chatItemView = `
  <div class="item text-warning">
    <div class="chat-btn text-secondary">
      <div class="item-btn-del">&#10008;</i></div>
    </div>
    <span class="username text-warning font-weight-bold">${newMessage.username}</span>:<span class="text text-secondary"> ${newMessage.text}</span>
  </div>`;
  item.innerHTML = chatItemView;
  chat.appendChild(item);
  item.getElementsByClassName('item-btn-del')[0].addEventListener('click', () => removeItem(newMessage, item));
}

// Добавить новое сообщение
document.getElementById('send-button').addEventListener('click', function () {
  let user = escapeHtml(document.getElementById('input-user').value);
  let message = escapeHtml(document.getElementById('input-message').value);
  axios.post('/api/chat/message', {
    username: user,
    text: message
  })
  .then(function (response) {
    data = JSON.parse(response.request.response);
    valid();
    document.getElementById('input-user').value = '';
    document.getElementById('input-message').value = '';
  })
  .catch(function (error) {
    console.log(error);
  });
  this.blur();
  });

//валидация нового сообщения
 function valid() {
  let fields = document.getElementsByClassName('input');
  if (fields[0].value && fields[1].value) {
        addMyItemToDOM(data[0]);
  }
  else {
    for (let i = 0; i < fields.length; i++) {
      if(!fields[i].value){
        fields[i].parentNode.classList.add("has-error");
        alert("Поле '" + fields[i].placeholder + "' не может быть пустым");
      } 
    }
  }
}
  
function addMyItemToDOM(newMessage) {
  let chat = document.getElementById('chat');
  let item = document.createElement('li'); 
  let chatItemView = `
  <div class="newIitem text-warning">
    <div class="chat-btn text-secondary">
      <div class="item-btn-edit">&#9998;</div>
      <div class="item-btn-del">&#10008;</div>
    </div>
    <span class="username text-warning font-weight-bold">${newMessage.username}</span>:<span class="text text-secondary"> ${newMessage.text}</span>
  </div>`;
  item.innerHTML = chatItemView;
  chat.appendChild(item);
  item.getElementsByClassName('item-btn-del')[0].addEventListener('click', () => removeItem(newMessage, item));
  item.getElementsByClassName('item-btn-edit')[0].addEventListener('click', () => editItem(newMessage, item));
}

// Удалить сообщение
function removeItem(newMessage, item) {
    if(confirm('Удалить сообщение?')){
      chat.removeChild(item);
      axios.delete(`api/chat/message/${newMessage.id}`);
    }
}

// Редактировать сообщение
function editItem(newMessage, item) {
    let chatItemView = `
      <div class="newIitem text-warning">
      <form class="form-inline">
        <div class="chat-btn text-secondary">
          <div class="item-btn-edit active">&#9998;</div>
          <div class="item-btn-del unactive">&#10008;</div>
        </div>

        <span class="username text-warning font-weight-bold">${newMessage.username}</span>: <div class="col-md-4"><input id="edit-message" class="input form-control" value="${newMessage.text}" type="text"></div>

        <button class="edit-button btn btn-warning" type="button" >Отправить</button>
        </form>
      </div>`;
  item.innerHTML = chatItemView;
  item.getElementsByClassName('edit-button')[0].addEventListener('click', () => editItemSend(newMessage, item));
  }

function editItemSend(newMessage, item) {
  let message = escapeHtml(document.getElementById('edit-message').value);
  if(!message) {
    alert("Поле 'Текст сообщения' не может быть пустым");
  } 
  else {
    axios.put(`api/chat/message/${newMessage.id}`, {
      text: message
    })
    .then(function (response) {
      let editMessage = response.data;
      data = JSON.parse(response.request.response);
      let chatItemView = `
        <div class="newIitem text-warning">
          <div class="chat-btn text-secondary">
            <div class="item-btn-edit">&#9998;</i></div>
            <div class="item-btn-del">&#10008;</div>
          </div>
          <span class="username text-warning font-weight-bold">${newMessage.username}</span>:<span class="text text-secondary"> ${editMessage.text}</span>
        </div>`;
      item.innerHTML = chatItemView;
      item.getElementsByClassName('item-btn-del')[0].addEventListener('click', () => removeItem(newMessage, item));
      item.getElementsByClassName('item-btn-edit')[0].addEventListener('click', () => editItem(newMessage, item));

    })
    .catch(function (error) {
      console.log(error);
    });
  }
}

  //экранирование символов
  let entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  };
  function escapeHtml (string) {
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
      return entityMap[s];
    });
  }
  
 
